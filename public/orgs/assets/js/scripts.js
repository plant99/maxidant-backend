
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    $.backstretch("assets/img/backgrounds/1.jpg");
    
    /*
        Login form validation
    */
    let $globalAlert = $('.globalAlert');
    $forms = $('form');
    for(var i=0;i< $forms.length;i++){
        var $form = $forms[i];
        console.log($form);
        var formId = $form.id;
        console.log(formId);
        $('#'+formId).ajaxForm(function(data, ts, jqxhr, form){
            var $button = $($(form[0]).find("button")[0]);
            if(form.attr('id') == 'login-form'){
                $button.html("Login");
            }else{
                $button.html("Signup");
            }
            $button.attr("disabled", false);
            console.log(data);
            if(data.success){
                location.href = '/orgs/dashboard';
            }else{
                $globalAlert.html(data.message);
                setTimeout(()=>{
                    $globalAlert.html(' ');
                }, 2000)
            }
        })
        $('#'+formId).submit((e)=>{
            var $button = $(e.target).find("button");
            console.log();
            $button.html("<i class='fa fa-circle-o-notch fa-spin'></i> Processing")
            $button.attr('disabled', true) ;
        })
    }

   //login


   //signup
    
});
