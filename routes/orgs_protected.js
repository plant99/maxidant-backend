const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const connection = require("../middlewares/db_init").getConnection();
/* Only for logged users*/
const saltRounds = 10;

const internalError = (res, err) => {
  if (err) {
    console.log(err);
    return res.json({ "status": "500", "success": false, "message": "Internal Server Error, retry!" });
  }
};

router.get('/sampleData', (req, res)=>{
  res.json({message:"sampleData for orgs"});
})
router.get('/dashboard', (req, res)=>{
	console.log("reached here");
	res.render('orgDashboard');
})
router.get('/undermycare', (req, res)=>{
	var orgId = req.session.orgId;
	connection.query("SELECT * FROM organisations WHERE id=?",orgId, (error, orgs)=>{
		if(error){
			return res.render('error',{message:'sorry, ISE!'});
		}
		let finalSetUsers = [];
		let users = orgs[0].usersUnderCare.split(',');
		if(users[0] == ''){
			users.pop();
		}
		console.log(users);
		var finalSetUser = {};
		var length = users.length;
		var count = 0;
		if(!users.length){
			return res.render('profile', {users: users})
		}
		for(var i=0; i<length; i++){
			 connection.query("SELECT * FROM users WHERE(id=?)", Number(users[i]), (error, usersFullDetails)=>{
			 	if(error){
			 		console.log(error);
			 	}
				finalSetUser = {};
				finalSetUser.name = usersFullDetails[0].name;
				finalSetUser.userId = usersFullDetails[0].id;
				finalSetUsers.push(finalSetUser);
				count++;
				if(count == users.length){
					return res.render('profile', {users: finalSetUsers});
				}
			});
		}
			
	})
})
module.exports = router;

function renderdash(alpha){
	return res.render('profile', {users:alpha});
}