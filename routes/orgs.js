"use strict";

const express = require("express");
const router = express.Router();
const md5 = require("md5");
const bcrypt = require("bcrypt");
const connection = require("../middlewares/db_init").getConnection();
const saltRounds = 10;



const internalError = (res, err) => {
  if (err) {
    console.log(err);
    return res.json({ "status": "500", "success": false, "message": "Internal Server Error. Please retry in some time." });
  }
};




// User login+signup handlers

// signup
router.post("/register", (req, res) => {
  let emailId = req.body.emailid;
  let password = req.body.password;
  let name = req.body.name;
  let contact = req.body.contact;
  let type = req.body.type;
  let city = req.body.city;
  let coordinates = req.body.coordinates;
  connection.query("SELECT emailId FROM organisations WHERE emailId=?", emailId, (error, users)=>{
    if(error){
      return res.json({success:false, message:"Internal server error"});
    }
    if(users.length){
      return res.json({success:false, message:"Sorry, org exists!"});
    }
  })
  console.log(emailId, password, name, contact);
  connection.query("INSERT INTO organisations (emailId, passwordHash, name, contact, type, city, coordinates) VALUES (?,?,?,?,?,?,?)", [emailId, password, name, contact, type, city, coordinates], (error,users)=>{
    //console.log(users);
    //console.log(error);
    if(!error){
     return res.json({success:true});
    }
    res.json({success:false, message:"Internal server error"});
    console.log(error);
  })
});

// login
router.post("/login", (req, res) => {
  if (req.session.isLoggedIn) {
    return res.json({ "status": 200, "success": false, "redirect": "/users/dashboard" });
  }

  const emailId = req.body.emailid;
  const password = req.body.password;

  if (!emailId || !password) {
    return res.json({ "status": 200, "success": false, "message": "Pass appropriate parameters!" });
  }

  connection.query("SELECT * FROM organisations WHERE (emailId = ?)", [ emailId ], (error, results, details) => {
    if (error) {
      res.json({message:"Internal server error"});
    }
    if (!results.length) {
      return res.json({ "status": 200, "success": false, "message": "Invalid credentials" });
    }

    const passwordUser = results[0].passwordHash;
    if (passwordUser != password) {
      return res.json({ "status": 200, "success": false, "message": "Invalid credentials" });
    }

    // const user = results[ 0 ];
    req.session.isLoggedIn = true;
    req.session.orgId = results[0].id;
    req.session.path = "/";
    res.cookie('orgId',req.session.orgId);
    res.json({ "status": 200, "success": true, "message": "Redirect to /users/dashboard" }); 
  });
});

module.exports = router;
