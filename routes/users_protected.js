const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const connection = require("../middlewares/db_init").getConnection();
/* Only for logged users*/
const saltRounds = 10;

const internalError = (res, err) => {
  if (err) {
    console.log(err);
    return res.json({ "status": "500", "success": false, "message": "Internal Server Error, retry!" });
  }
};

router.get('/sampleData', (req, res)=>{
  res.json({message:"sampleData"});
})
module.exports = router;
