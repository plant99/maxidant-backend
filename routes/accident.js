"use strict";
const express = require("express");
const router = express.Router();
const md5 = require("md5");
const bcrypt = require("bcrypt");
const connection = require("../middlewares/db_init").getConnection();
const saltRounds = 10;
/*
var text = require('textbelt');

text.send('8144855529', 'A sample text message!', undefined, function(err) {
  if (err) {
    console.log(err);
  }
});
*/

function geodistance(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}
const internalError = (res, err) => {
  if (err) {
    console.log(err);
    return res.json({ "status": "500", "success": false, "message": "Internal Server Error. Please retry in some time." });
  }
};
router.post('/declarefit/:userId', (req, res)=>{
  var userId = req.params.userId;
  var orgId = req.session.orgId;
  connection.query("DELETE FROM accidents WHERE(affectedUserId=?)",userId, (error)=>{
    if(error){
      return res.json({success:false, message:"ISE"});
    }
    connection.query("SELECT * FROM organisations WHERE (id=?)",orgId , (error, orgs)=>{
      if(error){
        console.log(error);
        return res.json({success:false, message:"ISE"});
      }
      var usersUnderCare = orgs[0].usersUnderCare.split(',');
      var indexToBeSpliced = usersUnderCare.indexOf(userId);
      var usersUnderCareNew = usersUnderCare.splice(indexToBeSpliced, 1);
      usersUnderCare = usersUnderCare.join(',');
      connection.query("UPDATE organisations SET usersUnderCare=? WHERE id=?", [usersUnderCare, orgId], (error, orgs)=>{
        if(error){
          console.log(error);
          return  res.json({success:false, message:"ISE"});
        }
        res.json({success:true, message:'The user is hereby declared fit!'});
      })
    })
  });
})
router.post('/accept/:userId', (req, res)=>{
  var userId = req.params.userId;
  connection.query("SELECT * FROM accidents WHERE affectedUserId=?",userId, (error, accidents)=>{
    if(accidents.length){
      if(accidents[0].concernedOrgsIds){
        res.json({success:true, message:'Thanks for your concern, another concerned org is attending the citizen'});
      }
    }
    var orgId = req.session.orgId;
    connection.query("SELECT * FROM organisations WHERE(id=?)",[orgId], (error, orgs)=>{
      //console.log(error);
      if(orgs[0]){
        let usersUnderCare = orgs[0].usersUnderCare;
        if(usersUnderCare){
          usersUnderCare = usersUnderCare.split(',');
          if(usersUnderCare.indexOf(userId) == -1){
            usersUnderCare.push(Number(userId));
            usersUnderCare = usersUnderCare.join(',').trim();
          }
        }else{
          usersUnderCare = [userId];
        }
        connection.query("UPDATE organisations SET usersUnderCare=? WHERE id=? ", [usersUnderCare, orgId], (error, orgs)=>{
          if(!error){
            res.json({success:true, message:"User is now accountable to you"});
            
            connection.query("UPDATE accidents SET concernedOrgsIds=?,status=? WHERE affectedUserId=?",[orgId,'attended', userId]);
            //or let there be a get request for every non-accidental user
          }else{
            console.log(error);
            res.json({success:false});
          }
        })
      }
    })
  })
  /*
    
    
  */
})

router.post("/create", (req, res)=>{
  let emailId = req.headers.emailid;
  let location = req.headers.location;
  let severity = req.headers.severity;
  console.log(emailId, location, severity);
  connection.query("SELECT * FROM users WHERE emailId=?", emailId, (error, users)=>{
    if(error){
      console.log(error);
      return res.json({success:false, message:"Internal server error"});
    }
    if(!users.length){
      return res.json({success:false, message: "No user with that e-mail!"});
    }
    let affectedUserId = users[0].id;
    console.log(affectedUserId);

    //create accident
    let coordSOS = {
      latitude: Number(location.split(',')[0]),
      longitude:Number(location.split(',')[1])
    }
    connection.query("SELECT id, coordinates FROM organisations WHERE 1", (error, orgs)=>{
      var finalOrgs = [];
      var distances = {};
      for(var i=0; i< orgs.length; i++){
        let user = orgs[i];
        let coordUser = {
          latitude:orgs[i].coordinates.split(',')[0],
          longitude:orgs[i].coordinates.split(',')[1]
        };

        var distance = geodistance(
          Number(coordUser.latitude), Number(coordUser.longitude), Number(coordSOS.latitude), Number(coordSOS.longitude)
        )
        orgs[i].distance = distance ;
        if(distance <= 20000){
          finalOrgs.push(orgs[i]);
        }

      }
      var count=0;
      for(var j=0; j<finalOrgs.length; j++){
        console.log(finalOrgs, 'hey');
        connection.query('SELECT * FROM sockets WHERE(orgId=?)',[Number(finalOrgs[j].id)], (error, sockets)=>{
          console.log(error, sockets);
          for(var k=0; k<sockets.length; k++){
            io.to(sockets[k].socketId).emit('new', {
                id: affectedUserId,
                location: location,
                severity: severity
            });
          }
        })
      }
    })

    connection.query("INSERT INTO accidents (affectedUserId, location, severity, status) VALUES(?,?,?,?)", [affectedUserId, location, severity, 'unattended'], (error)=>{
      if(error){
        console.log(error);
        return res.json({success:false, message:'Internal server error'});
      }
      return res.json({success:true, message:"Report would soon be sent to respective authorities"});
    })
    //send everybody and add to concerned orgs
    //set concerned orgs undercare kids

    //update status everytime someone responds to requests

    //set sockets to respond to those requests
    //on respective socket calls, disconnect the above two data
    
    /*
      geolib.getDistanceSimple(
          {latitude: 51.5103, longitude: 7.49347},
          {latitude: "51° 31' N", longitude: "7° 28' E"}
      );
    */

  })
})

module.exports = router;
