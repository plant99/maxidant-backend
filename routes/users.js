"use strict";

const express = require("express");
const router = express.Router();
const md5 = require("md5");
const bcrypt = require("bcrypt");
const connection = require("../middlewares/db_init").getConnection();
const saltRounds = 10;



const internalError = (res, err) => {
  if (err) {
    console.log(err);
    return res.json({ "status": "500", "success": false, "message": "Internal Server Error. Please retry in some time." });
  }
};

router.get('/details/:emailId', (req, res)=>{
   connection.query("SELECT * FROM users WHERE emailId=?",[req.params.emailId], (error, users)=>{
      if(!users.length){
        return res.json({user:{}, accident:{}, org:{}});
      }
      connection.query("SELECT * FROM accidents WHERE affectedUserId=?", users[0].id, (error, accidents)=>{
        if(!accidents.length){
          return res.json({user:users[0], accident:{}, org:{}});
        }
        if(accidents[0].status == "attended"){
          connection.query("SELECT name FROM organisations WHERE id=?", accidents[0].concernedOrgsIds, (error, orgs)=>{
            if(error){
              console.log(error);
            }
            res.json({user:users[0], accident:accidents[0], org: orgs[0]});
          })
        }
      })
   })
})



// User login+signup handlers

// signup
router.post("/register", (req, res) => {
  let emailId = req.headers.emailid;
  let password = req.headers.password;
  let name = req.headers.name;
  let contact = req.headers.contact;
  var emergencyContacts = req.headers.emergencycontacts;
  var bloodgroup = req.headers.bloodgroup;
  connection.query("SELECT emailId FROM users WHERE emailId=?", emailId, (error, users)=>{
    if(error){
      return res.json({success:false, message:"Internal server error"});
    }
    if(users.length){
      return res.json({success:false, message:"Sorry, user exists!"});
    }
  })
  console.log(emailId, password, name, contact);
  connection.query("INSERT INTO users (emailId, passwordHash, name, contact, emergencyContacts, bloodgroup) VALUES (?,?,?,?,?,?)", [emailId, password, name, contact,emergencyContacts, bloodgroup], (error,users)=>{
    //console.log(users);
    //console.log(error);
    if(!error){
     return res.json({success:true});
    }
    res.json({success:false, message:"Internal server error"});
  })
});
router.post('/otherDetails', (req, res)=>{
  var emergencyContacts = req.headers.emergencycontacts;
  var bloodgroup = req.headers.bloodgroup;
  var emailId = req.headers.emailid;
  console.log(emergencyContacts, bloodgroup, emailId);
  connection.query("UPDATE users SET emergencyContacts=?, bloodgroup=? WHERE emailId = ?",[emergencyContacts, bloodgroup, emailId], (error, users)=>{
    if(error){
      console.log(error);
      return res.json({success:false, message:"internal server error"});
    }
    return res.json({success:true, message:"Data updated!"});
  })

})

// login
router.post("/login", (req, res) => {
  if (req.session.isLoggedIn) {
    return res.json({ "status": 200, "success": false, "redirect": "/users/dashboard" });
  }

  const emailId = req.headers.emailid;
  const password = req.headers.password;

  if (!emailId || !password) {
    return res.json({ "status": 200, "success": false, "message": "Pass appropriate parameters!" });
  }

  connection.query("SELECT * FROM users WHERE (emailId = ?)", [ emailId ], (error, results, details) => {
    if (error) {
      res.json({message:"Internal server error"});
    }
    if (!results.length) {
      return res.json({ "status": 200, "success": false, "message": "Invalid credentials" });
    }

    const passwordUser = results[0].passwordHash;
    if (passwordUser != password) {
      return res.json({ "status": 200, "success": false, "message": "Invalid credentials" });
    }

    // const user = results[ 0 ];
    req.session.isLoggedIn = true;
    req.session.userId = results[0].id;
    req.session.path = "/";
      res.json({ "status": 200, "success": true, "message": "Redirect to /users/dashboard" }); 
  });
});

module.exports = router;
