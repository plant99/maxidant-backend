let admin = require("firebase-admin");
var serviceAccount = require("../config/one.json");

module.exports.initiateFMS = (req, res, next)=>{
	admin.initializeApp({
	  credential: admin.credential.cert(serviceAccount),
	  databaseURL: "https://sample-inout.firebaseio.com"
	});
	next();
}