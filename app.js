var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

const authenticate = require('./middlewares/authenticate').authenticate;
const secretString = require("./config/session_config.js").secretString;
const loginAndSignup = require("./routes/loginAndSignup");
const users = require("./routes/users");
const accident = require("./routes/accident");
const usersProtected = require("./routes/users_protected");
const orgs = require("./routes/orgs");
const orgsProtected = require("./routes/orgs_protected");
const session = require("express-session");
const initiateFMS = require('./middlewares/notification').initiateFMS;
var app = express();

app.use(session({
  "secret": secretString,
  "cookie": {
    "maxAge": 186000000,
  },
  "path": "/",
}));
app.set('view engine','ejs');
app.set('views', path.join(__dirname, '/views'));
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.get('/viewprofiles', (req, res)=>{
  res.render('userProfile',{title:'user',d1v:'none', d2v:'none'});
})
app.use('/accident', accident);
app.use('/orgs', orgs);
app.use("/users", users);
app.use(authenticate);
app.use("/users", usersProtected);
app.use('/orgs', orgsProtected);
/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({"message":'hey'});
        console.log("emssage senr");
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({"message":'hey'});
    console.log("emssage senr");
});


module.exports = app;
